# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from collections import defaultdict
from decimal import Decimal

from trytond.model import ModelView, Workflow, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, Not
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Journal(metaclass=PoolMeta):
    __name__ = 'account.payment.journal'
    processing_account = fields.Many2One('account.account',
        'Processing Account', states={
            'required': Bool(Eval('processing_journal')),
            },
        depends=['processing_journal'])
    processing_journal = fields.Many2One('account.journal',
        'Processing Journal', states={
            'required': Bool(Eval('processing_account')),
            },
        depends=['processing_account'])
    negotiating_receivable_account = fields.Many2One('account.account',
        'Negotiating Receivable Account',
        domain=[('type.receivable', '=', True)],
        states={'readonly': Not(Bool(Eval('processing_account')))},
        depends=['processing_account'])
    negotiating_payable_account = fields.Many2One('account.account',
        'Negotiating Payable Account',
        domain=[
            ('type.receivable', '=', False),
            ('type.payable', '=', False)
        ],
        states={
            'invisible': Not(Bool(Eval('negotiating_receivable_account'))),
        },
        depends=['negotiating_receivable_account'])
    negotiating_unreceivable_account = fields.Many2One('account.account',
        'Negotiating Unreceivable Account',
        domain=[('type.receivable', '=', True)],
        states={
            'invisible': Not(Bool(Eval('negotiating_receivable_account'))),
            'required': Bool(Eval('negotiating_receivable_account'))},
        depends=['negotiating_receivable_account'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.clearing_journal.states['required'] |= Bool(
            Eval('negotiating_receivable_account'))

    @fields.depends('processing_account')
    def on_change_processing_account(self):
        if not self.processing_account:
            self.negotiating_receivable_account = None
            self.negotiating_payable_account = None
            self.negotiating_unreceivable_account = None

    @fields.depends('negotiating_receivable_account')
    def on_change_negotiating_receivable_account(self):
        if not self.negotiating_receivable_account:
            self.negotiating_payable_account = None
            self.negotiating_unreceivable_account = None


class Payment(metaclass=PoolMeta):
    __name__ = 'account.payment'
    processing_move = fields.Many2One('account.move', 'Processing Move',
        readonly=True)
    negotiating_move = fields.Many2One('account.move', 'Negotiating Move',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.state.selection.extend([('negotiating', 'Negotiating')])
        cls._transitions |= set((
            ('processing', 'negotiating'),
            ('negotiating', 'succeeded'),
            ('negotiating', 'failed')))
        cls._buttons.update({
            'negotiate': {
                'invisible': (
                    (Eval('state') != 'processing')
                    | (Not(Bool(Eval('processing_move'))))),
                'icon': 'tryton-forward',
                'depends': ['state', 'processing_move'],
            }
            })
        for button in ('succeed', 'fail'):
            if cls._buttons.get(button).get('invisible'):
                cls._buttons[button]['invisible'] &= (
                    Eval('state') != 'negotiating')
            else:
                cls._buttons[button]['invisible'] = (
                    Eval('state') != 'negotiating')

    @classmethod
    @Workflow.transition('processing')
    def process(cls, payments, group):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')

        group = super(Payment, cls).process(payments, group)

        moves = []
        for payment in payments:
            move = payment.create_processing_move()
            if move:
                moves.append(move)
        if moves:
            Move.save(moves)
            cls.write(*sum((([m.origin], {'processing_move': m.id})
                        for m in moves), ()))
            Move.post(moves)

        to_reconcile = defaultdict(list)
        for payment in payments:
            if (payment.line
                    and not payment.line.reconciliation
                    and payment.processing_move):
                lines = [l for l in payment.processing_move.lines
                    if l.account == payment.line.account] + [payment.line]
                if not sum(l.debit - l.credit for l in lines):
                    to_reconcile[payment.party].extend(lines)
        for lines in list(to_reconcile.values()):
            Line.reconcile(lines)

        return group

    def create_processing_move(self, date=None):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Date = pool.get('ir.date')

        if not self.line:
            return
        if (not self.journal.processing_account
                or not self.journal.processing_journal):
            return

        if self.processing_move:
            return self.processing_move

        if date is None:
            date = Date.today()
        period = Period.find(self.company.id, date=date)

        # compatibility with account_bank_statement_payment
        move = Move(
            journal=self.journal.processing_journal,
            origin=self,
            date=date,
            period=period)

        line = self._get_move_line(self.line.account)
        counterpart = self._get_move_line(self.journal.processing_account,
            receivable=True)
        counterpart.maturity_date = self.date

        move.lines = (line, counterpart)
        return move

    def _get_move_line(self, account, receivable=False):
        pool = Pool()
        Currency = pool.get('currency.currency')
        Line = pool.get('account.move.line')

        clearing_percent = getattr(
            self.journal, 'clearing_percent', Decimal(1)) or Decimal(1)
        processing_amount = self.amount * clearing_percent

        local_currency = self.journal.currency == self.company.currency
        if not local_currency:
            with Transaction().set_context(date=self.date):
                local_amount = Currency.compute(
                    self.journal.currency, processing_amount,
                    self.company.currency)
        else:
            local_amount = self.company.currency.round(processing_amount)

        line = Line()
        if not receivable:
            if self.kind == 'payable':
                line.debit, line.credit = local_amount, 0
            else:
                line.debit, line.credit = 0, local_amount
            if not local_currency:
                line.amount_second_currency = processing_amount
                line.second_currency = self.journal.currency
        else:
            if self.kind == 'payable':
                line.debit, line.credit = 0, local_amount
            else:
                line.debit, line.credit = local_amount, 0
            if not local_currency:
                line.amount_second_currency = -processing_amount
                line.second_currency = self.journal.currency
        line.account = account
        line.party = self.line.party if account.party_required else None

        return line

    @classmethod
    @ModelView.button
    @Workflow.transition('succeeded')
    def succeed(cls, payments):
        pool = Pool()
        Line = pool.get('account.move.line')

        super(Payment, cls).succeed(payments)

        for payment in payments:
            if (payment.journal.processing_account
                    and payment.journal.processing_account.reconcile
                    and payment.processing_move
                    and payment.journal.clearing_account
                    and payment.journal.clearing_account.reconcile
                    and payment.clearing_move):
                to_reconcile = defaultdict(list)
                lines = (payment.processing_move.lines
                    + payment.clearing_move.lines)
                if payment.negotiating_move:
                    lines += payment.negotiating_move.lines
                for line in lines:
                    if line.account.reconcile and not line.reconciliation:
                        key = (
                            line.account.id,
                            line.party.id if line.party else None)
                        to_reconcile[key].append(line)
                for lines in list(to_reconcile.values()):
                    if not sum((l.debit - l.credit) for l in lines):
                        Line.reconcile(lines)

    def _get_clearing_move(self, date=None):
        move = super()._get_clearing_move(date=date)
        if move and self.negotiating_move:
            payable_account, = [line.account
                for line in self.negotiating_move.lines if line.credit
                and line.account != self.journal.processing_account
            ]
            line1 = self._get_move_line(
                self.journal.negotiating_receivable_account)
            line2 = self._get_move_line(payable_account, receivable=True)
            move.lines = (line1, line2)
        elif move and self.processing_move:
            for line in move.lines:
                if line.account == self.line.account:
                    line.account = self.journal.processing_account
                    line.party = (self.line.party
                        if line.account.party_required else None)
        return move

    @classmethod
    @ModelView.button
    @Workflow.transition('failed')
    def fail(cls, payments):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Reconciliation = pool.get('account.move.reconciliation')

        super(Payment, cls).fail(
            [p for p in payments if not p.negotiating_move])

        to_delete = []
        to_reconcile = defaultdict(lambda: defaultdict(list))
        to_unreconcile = []
        to_post = []
        for payment in payments:
            if payment.processing_move and not payment.negotiating_move:
                if payment.processing_move.state == 'draft':
                    to_delete.append(payment.processing_move)
                    for line in payment.processing_move.lines:
                        if line.reconciliation:
                            to_unreconcile.append(line.reconciliation)
                else:
                    cancel_move = payment.processing_move.cancel()
                    to_post.append(cancel_move)
                    for line in (payment.processing_move.lines
                            + cancel_move.lines):
                        if line.reconciliation:
                            to_unreconcile.append(line.reconciliation)
                        if line.account.reconcile:
                            to_reconcile[payment.party][line.account].append(
                                line)

            if payment.negotiating_move:
                negotiate_fail_move = payment.create_negotiating_failed_move()
                negotiate_fail_move.save()
                to_post.append(negotiate_fail_move)

                if payment.clearing_move and \
                        payment.clearing_move.state == 'draft':
                    to_post.append(payment.clearing_move)

                for line in (payment.negotiating_move.lines
                        + negotiate_fail_move.lines
                        + (payment.clearing_move
                            and payment.clearing_move.lines or tuple())):
                    unreceiv = payment.journal.negotiating_unreceivable_account
                    if not line.reconciliation and line.account.reconcile and \
                            line.account != unreceiv:
                        to_reconcile[payment.party][line.account].append(
                            line)

        if to_unreconcile:
            Reconciliation.delete(to_unreconcile)
        if to_delete:
            Move.delete(to_delete)
        if to_post:
            Move.post(to_post)
        for party in to_reconcile:
            for lines in list(to_reconcile[party].values()):
                Line.reconcile(lines)

        cls.write([p for p in payments if not p.negotiating_move], {
            'processing_move': None
        })

    def create_negotiating_failed_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Date = pool.get('ir.date')

        date = Date.today()
        period = Period.find(self.company.id, date=date)

        payable_account, = [line.account
            for line in self.negotiating_move.lines
            if line.credit and line.account != self.journal.processing_account
        ]
        move = Move(
            journal=self.journal.processing_journal,
            origin=self,
            date=date,
            period=period)
        if self.kind == 'payable':
            negotiate_account, = [line.account
                for line in self.negotiating_move.lines
                if line.credit
                and line.account != self.journal.negotiating_receivable_account
            ]
        else:
            negotiate_account, = [
                line.account for line in self.negotiating_move.lines
                if line.debit
                and line.account != self.journal.negotiating_receivable_account
            ]
        lines = []
        if self.state != 'succeeded':
            line1 = self._get_move_line(
                self.journal.negotiating_receivable_account)
            line2 = self._get_move_line(payable_account, receivable=True)
            lines.extend([line1, line2])
        line3 = self._get_move_line(negotiate_account)
        line4 = self._get_move_line(
            self.journal.negotiating_unreceivable_account, receivable=True)
        lines.extend([line3, line4])

        move.lines = lines
        return move

    @classmethod
    @ModelView.button_action(
        'account_payment_processing.wizard_payment_negotiate')
    def negotiate(cls, records):
        pass

    @classmethod
    @Workflow.transition('negotiating')
    def negotiated(cls, records, payable_account, clearing_account):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')
        Date = pool.get('ir.date')
        Reconciliation = pool.get('account.move.reconciliation')

        date = Date.today()
        moves = []
        to_reconcile = defaultdict(lambda: defaultdict(list))
        to_unreconcile = []
        for payment in records:
            period = Period.find(payment.company.id, date=date)

            move = Move(
                journal=payment.journal.processing_journal,
                origin=payment,
                date=date,
                period=period)

            line = payment._get_move_line(payment.journal.processing_account)
            line_receivable = payment._get_move_line(
                payment.journal.negotiating_receivable_account,
                receivable=True)
            line_payable = payment._get_move_line(
                payment.journal.negotiating_payable_account)
            line_negotiate_account = payment._get_move_line(
                clearing_account, receivable=True)

            move.lines = (line, line_receivable, line_payable,
                line_negotiate_account)
            move.save()
            payment.negotiating_move = move
            moves.append(move)

            discard_accounts = {payment.journal.negotiating_payable_account,
                payment.journal.negotiating_receivable_account}
            for line in (payment.processing_move.lines + move.lines):
                if line.account.reconcile and not line.reconciliation and \
                        line.account not in discard_accounts:
                    to_reconcile[payment.party][line.account].append(line)

        cls.save(records)
        if moves:
            Move.post(moves)

        if to_unreconcile:
            Reconciliation.delete(to_unreconcile)
        for party in to_reconcile:
            for lines in to_reconcile[party].values():
                Line.reconcile(lines)


class PaymentNegotiateData(ModelView):
    """Account Payment Data"""
    __name__ = 'account.payment.negotiate.data'

    clearing_account = fields.Many2One('account.account',
        'Negotiating Account of bank',
        required=True, domain=[
            ('type.payable', '=', False),
            ('type.receivable', '=', False)
        ])
    payable_account = fields.Many2One('account.account',
        'Negotiating Payable Account', required=True,
        domain=[
            ('type.receivable', '=', False),
            ('type.stock', '=', False),
            ('type.payable', '=', False),
            ('type.revenue', '=', False),
            ('type.expense', '=', False)])


class PaymentNegotiate(Wizard):
    """Account Payment Negotiate"""
    __name__ = 'account.payment.negotiate'

    start = StateTransition()
    negotiate = StateView('account.payment.negotiate.data',
        'account_payment_processing.payment_negotiate_data_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'do_', 'tryton-ok', default=True)])
    do_ = StateTransition()

    def transition_start(self):
        pool = Pool()
        Payment = pool.get('account.payment')

        payments = Payment.browse(Transaction().context['active_ids'])

        for payment in payments:
            if payment.state != 'processing':
                return 'end'
            if payment.journal and \
                    not payment.journal.negotiating_receivable_account:
                raise UserError(gettext('account_payment_processing.'
                    'msg_missing_negotiating_receivable_account',
                    journal=payment.journal.rec_name,
                    payment=payment.rec_name))
        return 'negotiate'

    def default_negotiate(self, fields):
        pool = Pool()
        Payment = pool.get('account.payment')

        payments = Payment.browse(Transaction().context['active_ids'])
        journals = list(set(p.journal for p in payments))
        if len(journals) == 1:
            journals, = journals
            return {
                'clearing_account': journals.clearing_account.id,
                'payable_account': (journals.negotiating_payable_account.id
                    if journals.negotiating_payable_account else None)
            }
        return {}

    def transition_do_(self):
        Payment = Pool().get('account.payment')
        payments = Payment.browse(Transaction().context['active_ids'])
        Payment.negotiated(payments, self.negotiate.payable_account,
            self.negotiate.clearing_account)
        return 'end'
