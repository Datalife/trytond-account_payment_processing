====================================
Account Payment Negotiating Scenario
====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from decimal import Decimal
    >>> from datetime import datetime


Install account_payment_processing::

    >>> config = activate_modules('account_payment_processing')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Date::

    >>> now = datetime.now()


Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> cash = accounts['cash']


Create processing accounts::

    >>> Account = Model.get('account.account')
    >>> processing = Account()
    >>> processing.name = 'Customers Processing Payments'
    >>> processing.parent = receivable.parent
    >>> processing.type = receivable.type
    >>> processing.reconcile = True
    >>> processing.party_required = True
    >>> processing.deferral = True
    >>> processing.save()


Get journals::

    >>> Journal = Model.get('account.journal')
    >>> journal_revenue, = Journal.find([('code', '=', 'REV')])
    >>> journal_cash, = Journal.find([('code', '=', 'CASH')])


Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')
    >>> payment_journal = PaymentJournal(
    ...     name='Processing',
    ...     process_method='manual',
    ...     clearing_journal=journal_cash,
    ...     clearing_account=cash,
    ...     processing_journal=journal_revenue,
    ...     processing_account=processing)
    >>> payment_journal.save()


Create party::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create moves to pay for the customer::

    >>> Move = Model.get('account.move')
    >>> move = Move()
    >>> move.period = period
    >>> move.journal = journal_revenue
    >>> move.date = period.start_date
    >>> line = move.lines.new()
    >>> line.account = revenue
    >>> line.credit = Decimal(100)
    >>> line = move.lines.new()
    >>> line.account = receivable
    >>> line.debit = Decimal(100)
    >>> line.party = customer
    >>> line.maturity_date = now
    >>> move.click('post')
    >>> move2, = move.duplicate()
    >>> move2.click('post')


Create customer payment::

    >>> Payment = Model.get('account.payment')
    >>> line, = [l for l in move.lines if l.account.type.receivable]
    >>> pay_line = Wizard('account.move.line.pay', [line])
    >>> pay_line.execute('next_')
    >>> pay_line.form.journal = payment_journal
    >>> pay_line.execute('next_')
    >>> payment, = Payment.find([('state', '=', 'draft')])
    >>> payment.amount
    Decimal('100')
    >>> payment.click('submit')
    >>> process_payment = Wizard('account.payment.process', [payment])
    >>> process_payment.execute('process')
    >>> payment.reload()
    >>> negotiate_payment = Wizard('account.payment.negotiate', [payment])
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Must define Negotiating Receivable Account on Journal "Processing" in order to negotiate Payment "1". - 


Create negotiating accounts::

    >>> n_receivable_account = Account()
    >>> n_receivable_account.name = 'Negotiating Receivable account'
    >>> n_receivable_account.parent = receivable.parent
    >>> n_receivable_account.type = receivable.type
    >>> n_receivable_account.reconcile = True
    >>> n_receivable_account.party_required = True
    >>> n_receivable_account.save()
    
    >>> n_payable_account = Account()
    >>> n_payable_account.name = 'Negotiating Payable account'
    >>> n_payable_account.parent = cash.parent
    >>> n_payable_account.type = cash.type
    >>> n_payable_account.save()
    
    >>> n_unreceivable_account = Account()
    >>> n_unreceivable_account.name = 'Negotiating Unreceivable account'
    >>> n_unreceivable_account.parent = receivable.parent
    >>> n_unreceivable_account.type = receivable.type
    >>> n_unreceivable_account.save()
    
    >>> negotiate_account = Account()
    >>> negotiate_account.name = 'Negotiate account'
    >>> negotiate_account.parent = cash.parent
    >>> negotiate_account.type = cash.type
    >>> negotiate_account.save()


Add negotiating accounts to journal::

    >>> payment_journal.negotiating_receivable_account = n_receivable_account
    >>> payment_journal.negotiating_payable_account = n_payable_account
    >>> payment_journal.negotiating_unreceivable_account = n_unreceivable_account
    >>> payment_journal.save()


Negotiate payment::

    >>> Line = Model.get('account.move.line')
    >>> payment.kind
    'receivable'
    >>> negotiate_payment = Wizard('account.payment.negotiate', [payment])
    >>> negotiate_payment.form.clearing_account = negotiate_account
    >>> negotiate_payment.execute('do_')
    >>> payment.negotiating_move != None
    True
    >>> len (payment.negotiating_move.lines)
    4
    >>> line1, = [l for l in payment.negotiating_move.lines if l.account == n_receivable_account]
    >>> line2, = [l for l in payment.negotiating_move.lines if l.account == n_payable_account]
    >>> line3, = [l for l in payment.negotiating_move.lines if l.account == negotiate_account]
    >>> line3, = [l for l in payment.negotiating_move.lines if l.account == processing]
    >>> payment.click('succeed')
    >>> len(payment.clearing_move.lines)
    2
    >>> payment.clearing_move.state
    'draft'
    >>> line1, = [l for l in payment.clearing_move.lines if l.account == n_receivable_account]
    >>> line2, = [l for l in payment.clearing_move.lines if l.account == n_payable_account]
    >>> line1.credit > 0 and line1.debit == 0
    True
    >>> line2.credit == 0 and line2.debit > 0
    True
    >>> payment.click('fail')
    >>> payment.reload()
    >>> payment.state
    'failed'
    >>> payment.processing_move and payment.negotiating_move and True or False
    True
    >>> payment.clearing_move.state
    'posted'
    >>> fail_line, = Line.find([
    ...     ('move.origin', '=', 'account.payment,%s' % payment.id),
    ...     ('account', '=', n_unreceivable_account.id)])
    >>> len(fail_line.move.lines)
    2


Negotiate second payment with custom payable account and fail::

    >>> n_payable_account2 = Account()
    >>> n_payable_account2.name = 'Negotiating Payable account 2'
    >>> n_payable_account2.parent = cash.parent
    >>> n_payable_account2.type = cash.type
    >>> n_payable_account2.save()
    >>> line, = [l for l in move2.lines if l.account == receivable]
    >>> pay_line = Wizard('account.move.line.pay', [line])
    >>> pay_line.execute('next_')
    >>> pay_line.form.journal = payment_journal
    >>> pay_line.execute('next_')
    >>> payment, = Payment.find([('state', '=', 'draft')])
    >>> payment.click('submit')
    >>> process_payment = Wizard('account.payment.process', [payment])
    >>> process_payment.execute('process')
    >>> payment.reload()
    >>> negotiate_payment = Wizard('account.payment.negotiate', [payment])
    >>> negotiate_payment.form.clearing_account = negotiate_account
    >>> negotiate_payment.form.payable_account = n_payable_account2
    >>> negotiate_payment.execute('do_')
    >>> payment.click('fail')
    >>> fail_line, = Line.find([
    ...     ('move.origin', '=', 'account.payment,%s' % payment.id),
    ...     ('account', '=', n_unreceivable_account.id)])
    >>> len(fail_line.move.lines)
    4
    >>> move = fail_line.move
    >>> line1, = [l for l in move.lines if l.account == n_receivable_account]
    >>> line2, = [l for l in move.lines if l.account == n_payable_account]
    >>> line3, = [l for l in move.lines if l.account == n_unreceivable_account]
    >>> line4, = [l for l in move.lines if l.account == negotiate_account]